/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/29 19:59:22 by lburlach          #+#    #+#             */
/*   Updated: 2017/11/10 16:15:35 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Allocates (with malloc(3)) and returns a “fresh” memory area.
** The memory allocated is initialized to 0. If the allocation fails,
** the function returns NULL.
*/

void	*ft_memalloc(size_t size)
{
	size_t	i;
	void	*malloc_pointer;

	malloc_pointer = malloc(size);
	if (malloc_pointer == NULL)
		return (NULL);
	i = 0;
	while (i < size)
		((unsigned char *)malloc_pointer)[i++] = 0;
	return (malloc_pointer);
}
