/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/17 13:01:10 by lburlach          #+#    #+#             */
/*   Updated: 2017/12/22 16:59:36 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# define BUFF_SIZE 42
# include "libft.h"
# include <fcntl.h>
# include <sys/types.h>
# include <sys/stat.h>

typedef	struct		s_ffs
{
	void			*d;
	size_t			s;
	size_t			pos;
	int				fd;
	struct s_ffs	*next;
}					t_ffs;

int					get_next_line(const int fd, char **line);

# define DAT ((char *)data - buf)
# define CP cp->pos
# define READ_L read_lines(&s_head, &tmp, fd, line)
# define CR (char *)

#endif
