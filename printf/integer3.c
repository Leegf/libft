/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   integer3.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/04 15:44:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/02/04 15:44:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** self-explanatory to the utmost meaning of the word "explanatory"
*/

int		costul1(t_flags *flags, t_list **head, char *n)
{
	if ((((*flags).conv == 'u' || (*flags).conv == 'U') || check_flags(*flags))
		&& (*flags).space && (*flags).width <= (int)ft_strlen(n))
	{
		if (!(*flags).minus && !(*flags).zero && (*flags).hash
			&& ((*flags).conv == 'x' || (*flags).conv == 'X'))
			hex_fix(flags, head, n);
		return (0);
	}
	if ((*flags).space && (*flags).hash && (*flags).width > (*flags).prec
		&& (((*flags).conv == 'x') || (*flags).conv == 'X')
		&& (*flags).prec == 1)
	{
		ft_lst_push_back(head, " ", 1);
		return (1);
	}
	return (1);
}

int		costul3(t_flags *flags, t_list **head, char sign)
{
	if ((*flags).space && sign != '-' && !(*flags).plus && (*flags).conv != 'u'
		&& (*flags).conv != 'U' && (*flags).conv != 'o' && (*flags).conv != 'O'
		&& (*flags).conv != 'x' && (*flags).conv != 'X' && (*flags).conv != 'p')
	{
		ft_lst_push_back(head, " ", 1);
		return (1);
	}
	else if ((*flags).width > 1 && (*flags).width > (*flags).prec
			&& (*flags).space && sign != '-' && !(*flags).minus
			&& ((*flags).conv == 'o' || (*flags).conv == 'O' ||
			(*flags).conv == 'u' || (*flags).conv == 'U' || (*flags).conv == 'x'
				|| (*flags).conv == 'X' || (*flags).conv == 'p'))
		return (2);
	return (0);
}

void	costul2(t_flags *flags, t_list **head, char *n)
{
	if ((*flags).hash && (*flags).space &&
		((*flags).prec == 0 || ((*flags).prec == -1 && (*flags).point)) &&
		((*flags).conv == 'x' || (*flags).conv == 'X'))
		ft_lst_push_back(head, " ", 1);
	if ((*flags).zero)
	{
		if ((*flags).conv == 'p')
			hex_fix(flags, head, n);
		if ((*flags).prec == -1)
			ft_lst_push_back(head, "0", 1);
		else if ((*flags).space && (*flags).hash && (*flags).zero &&
				((*flags).conv == 'x' || (*flags).conv == 'X'))
			(*flags).hash = (*flags).hash;
		else
			ft_lst_push_back(head, " ", 1);
	}
	else if ((*flags).hash && check_flags(*flags) && (*flags).conv != 'p' &&
			n[0] != '0' && !((*flags).space && (*flags).hash &&
							((*flags).conv == 'o' || (*flags).conv == 'O')))
		hex_fix(flags, head, n);
	else
		ft_lst_push_back(head, " ", 1);
}

/*
** I dunno why I named like that. It looks up space, plus
** or minus sign and adds when it's needed.
*/

void	make_zero_work(t_flags *flags, t_list **head, char sign, char *n)
{
	int hm;

	if (sign == '7')
		return ;
	if (!costul1(flags, head, n))
		return ;
	if ((hm = costul3(flags, head, sign)))
	{
		if (hm == 1)
			return ;
		costul2(flags, head, n);
		if (!(*flags).zero && (*flags).conv == 'p')
			hex_fix(flags, head, n);
	}
	else if ((*flags).plus && sign != '-' && (*flags).conv != 'U'
			&& (*flags).conv != 'u' && (*flags).conv != 'o'
			&& (*flags).conv != 'O'
			&& (*flags).conv != 'x' && (*flags).conv != 'X'
			&& (*flags).conv != 'p')
		ft_lst_push_back(head, "+", 1);
	else if (sign == '-')
		ft_lst_push_back(head, "-", 1);
}

/*
** fek_up, right
*/

int		fk_p(t_flags flags, char sign)
{
	if (flags.width > 1 && flags.width > flags.prec && flags.space
		&& sign != '-' && !flags.minus
		&& (flags.conv == 'o' || flags.conv == 'O' || flags.conv == 'u'
			|| flags.conv == 'U' || flags.conv == 'x' || flags.conv == 'X'
			|| flags.conv == 'p'))
		return (1);
	return (0);
}
