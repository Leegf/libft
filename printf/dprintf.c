/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dprintf.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/04 16:39:37 by lburlach          #+#    #+#             */
/*   Updated: 2018/02/04 16:40:23 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

ssize_t		parsing_d(int fd, va_list ax, char *fmt, t_list **head)
{
	char	*buff_str;
	t_flags	flags;
	ssize_t	len;
	int		rez;

	buff_str = NULL;
	while (*fmt)
	{
		initialise_flags(&flags);
		parse_simple_str(&fmt, head, &flags);
		parse_flags(&flags, &fmt);
		parse_width_n_prec(&flags, &fmt, ax);
		parse_length_n_conversion(&flags, &fmt);
		writing_variadic_args(ax, flags, head);
	}
	len = allocate(*head, &buff_str);
	if (len == -1)
		return (-1);
	ft_lst_clear(head);
	rez = write(fd, buff_str, len);
	ft_strdel(&buff_str);
	return (rez == -1 ? -1 : len);
}

/*
** Pls, work.
*/

int			ft_dprintf(int fd, const char *format, ...)
{
	va_list		ax;
	t_list		*buff;
	int			res;

	buff = NULL;
	va_start(ax, format);
	res = parsing_d(fd, ax, (char *)format, &buff);
	va_end(ax);
	return (res);
}
