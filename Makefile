# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/11/05 15:31:08 by lburlach          #+#    #+#              #
#    Updated: 2018/03/14 14:09:42 by lburlach         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY : all clean fclean re norm

NAME=libft.a
SRCS_c=ft_atoi.o ft_bzero.o ft_isalnum.o ft_isalpha.o ft_isascii.o ft_isdigit.o ft_isprint.o ft_itoa.o ft_lstadd.o ft_lstdel.o ft_lstdelone.o ft_lstiter.o ft_lstmap.o ft_lstnew.o ft_memalloc.o ft_memccpy.o ft_memchr.o ft_memcmp.o ft_memcpy.o ft_memdel.o ft_memmove.o ft_memset.o ft_putchar.o ft_putchar_fd.o ft_putendl.o ft_putendl_fd.o ft_putnbr.o ft_putnbr_fd.o ft_putstr.o ft_putstr_fd.o ft_strcat.o ft_strchr.o ft_strclr.o ft_strcmp.o ft_strcpy.o ft_strdel.o ft_strdup.o ft_strequ.o ft_striter.o ft_striteri.o ft_strjoin.o ft_strlcat.o ft_strlen.o ft_strmap.o ft_strmapi.o ft_strncat.o ft_strncmp.o ft_strncpy.o ft_strnequ.o ft_strnew.o ft_strnstr.o ft_strrchr.o ft_strsplit.o ft_strstr.o ft_strsub.o ft_strtrim.o ft_tolower.o ft_toupper.o ft_lst_at.o ft_lst_merge.o ft_lst_push_back.o ft_lst_push_front.o ft_lst_reverse.o ft_lst_size.o ft_lst_clear.o get_next_line.o str_from_lsts.o ft_strlenuu.o str_from_lstsu.o find_pos.o
PRINTF_SRCS = printf/parse.o printf/parse2.o printf/parse3.o printf/char.o printf/char2.o printf/string.o printf/string2.o printf/string3.o printf/integer.o printf/itoa_base.o printf/integer2.o printf/uinteger.o printf/octal.o printf/parse4.o printf/integer3.o printf/integer5.o printf/dprintf.o
CC = gcc
OBJ = $(SRCS_c)
OBJ += $(PRINTF_SRCS)
FLAGS= -Wall -Wextra -Werror -O2
INC = ./includes

all: $(NAME)

$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)

clean:
	/bin/rm -f $(OBJ)

fclean: clean
	/bin/rm -f $(NAME)

re: fclean all

norm:
	Norminette -R CheckForbiddenSourceHeader *.c
	Norminette -R CheckForbiddenSourceHeader *.h 

$(OBJ) : %.o: %.c
	$(CC) -I $(INC) -c $(FLAGS) $< -o $@
