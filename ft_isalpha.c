/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalpha.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 17:28:00 by lburlach          #+#    #+#             */
/*   Updated: 2017/11/09 19:32:21 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The isalpha() function tests for any character for which isupper(3)
** or islower(3) is true.  The value of the
** argument must be representable as an unsigned char or the value of EOF.
** The isalpha() function returns zero if the character tests false and
** returns non-zero if the character tests true.
*/

int		ft_isalpha(int c)
{
	unsigned char tmp;

	if (c > 255 || c < 0)
		return (0);
	tmp = (unsigned char)c;
	if ((tmp >= 'a' && tmp <= 'z')
			|| (tmp >= 'A' && tmp <= 'Z'))
		return (1);
	return (0);
}
